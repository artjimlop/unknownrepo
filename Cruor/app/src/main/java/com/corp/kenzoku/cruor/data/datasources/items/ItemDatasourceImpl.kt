package com.corp.kenzoku.cruor.data.datasources.items

import com.corp.kenzoku.cruor.data.dtos.SearchItemResponse
import com.corp.kenzoku.cruor.data.net.ItemApiService
import io.reactivex.Single

class ItemDatasourceImpl constructor(private val itemApiService: ItemApiService): ItemDatasource {

    override fun searchItem(query: String): Single<SearchItemResponse> {
        return itemApiService.getItemsByQuery(query)
    }
}