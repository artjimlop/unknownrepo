package com.corp.kenzoku.cruor.presentation.views

import com.corp.kenzoku.cruor.presentation.models.VisitedItemModel

interface ItemDetailView : BaseView {
    fun showItemInfo(visitedItemModel: VisitedItemModel)
    fun switchFavoriteStatus()
}