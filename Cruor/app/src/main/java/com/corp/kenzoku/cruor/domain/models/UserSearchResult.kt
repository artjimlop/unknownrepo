package com.corp.kenzoku.cruor.domain.models

data class UserSearchResult(val id: String,
                            val avatar: String,
                            val username: String,
                            val profileUrl: String,
                            val isFavorite: Boolean,
                            val alreadyVisited: Boolean,
                            var isBestMatch: Boolean = false)
