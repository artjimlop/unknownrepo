package com.corp.kenzoku.cruor.data.datasources.items

import com.corp.kenzoku.cruor.data.dtos.VisitedItemEntity
import io.reactivex.Completable
import io.reactivex.Single

interface VisitedItemDatasource {

    fun markItemAsVisited(visitedItemEntity: VisitedItemEntity): Completable
    fun loadItemById(itemId: String): Single<VisitedItemEntity>
    fun loadVisitedItems(): Single<List<VisitedItemEntity>>
    fun updateItemFavorite(itemId: String, isFavorite: Boolean): Completable
}