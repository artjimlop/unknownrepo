package com.corp.kenzoku.cruor.data.repositories

import com.corp.kenzoku.cruor.domain.models.User
import io.reactivex.Single

interface ItemRepository {

    fun searchUsers(query: String): Single<List<User>>
}