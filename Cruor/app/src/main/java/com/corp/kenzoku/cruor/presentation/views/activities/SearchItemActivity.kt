package com.corp.kenzoku.cruor.presentation.views.activities

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import com.corp.kenzoku.cruor.R
import com.corp.kenzoku.cruor.presentation.BaseActivity
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel
import com.corp.kenzoku.cruor.presentation.presenters.SearchItemPresenter
import com.corp.kenzoku.cruor.presentation.views.SearchItemView
import com.corp.kenzoku.cruor.presentation.views.adapters.ItemsAdapter
import kotlinx.android.synthetic.main.activity_search_item.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.kodein.di.generic.instance


class SearchItemActivity : BaseActivity(), SearchItemView, SearchView.OnQueryTextListener {
    private val lastSearchId: String = "last_search"
    private val presenter: SearchItemPresenter by instance()
    private var lastSearch: String? = null
    private lateinit var itemsAdapter: ItemsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_item)
        presenter.initialize(this)
        setupItemsView()
        setUpToolbar(appBar.toolbar, false)
    }

    override fun onResume() {
        super.onResume()
        lastSearch?.let { presenter.searchItem(it) }
    }

    private fun setupItemsView() {
        itemsAdapter = ItemsAdapter { item ->
            presenter.onItemClicked(item)
        }
        itemList.layoutManager = LinearLayoutManager(this)
        itemList.adapter = itemsAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search_item_menu, menu)

        val search = menu!!.findItem(R.id.actionSearch)
        val searchView = search.actionView as SearchView
        searchView.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }

    override fun showResults(searchResults: List<SearchItemModel>) {
        itemsAdapter.setItems(searchResults)
        itemsAdapter.notifyDataSetChanged()
    }

    override fun showError() {
        showErrorFeedback(searchItemRootView)
    }

    override fun goToItemDetail(item: SearchItemModel) {
        val intent = ItemDetailActivity.goToItemDetail(this@SearchItemActivity, item.id)
        startActivity(intent)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        this.lastSearch = query
        presenter.searchItem(query!!)
        return true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState!!.putString(lastSearchId, lastSearch)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        lastSearch = savedInstanceState!!.getString(lastSearchId)
    }
}
