package com.corp.kenzoku.cruor.data.dtos

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "visited_items")
class VisitedItemEntity(@ColumnInfo(name = "id")
                        @PrimaryKey var id: String,
                        @ColumnInfo(name = "avatar") var avatar: String,
                        @ColumnInfo(name = "username") var username: String,
                        @ColumnInfo(name = "profileUrl") var profileUrl: String,
                        @ColumnInfo(name = "isFavorite") var isFavorite: Boolean)