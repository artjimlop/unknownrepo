package com.corp.kenzoku.cruor

import android.app.Application
import com.corp.kenzoku.cruor.di.injector
import org.kodein.di.KodeinAware

class GitHubApplication : Application(), KodeinAware {
    override val kodein = injector(this@GitHubApplication)
}