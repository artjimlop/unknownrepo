package com.corp.kenzoku.cruor.presentation.models

data class SearchItemModel(val id: String,
                           val avatar: String,
                           val username: String,
                           val profileUrl: String,
                           val isFavorite: Boolean,
                           val alreadyVisited: Boolean,
                           var isBestMatch: Boolean = false)
