package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import com.corp.kenzoku.cruor.domain.models.VisitedUser
import io.reactivex.Single

class LoadUserByIdUseCase constructor(private val visitedItemRepository: VisitedItemRepository):
        LoadUserById {

    override fun loadUserById(userId: String): Single<VisitedUser> {
        return visitedItemRepository.loadVisitedUser(userId)
    }
}