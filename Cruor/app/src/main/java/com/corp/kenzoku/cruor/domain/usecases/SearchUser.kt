package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import io.reactivex.Single

interface SearchUser {

    fun searchUser(query: String): Single<List<UserSearchResult>>
}