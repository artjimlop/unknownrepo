package com.corp.kenzoku.cruor.data.net

import com.corp.kenzoku.cruor.data.dtos.SearchItemResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ItemApiService {
    @GET("search/users") fun getItemsByQuery(@Query("q") query: String): Single<SearchItemResponse>
}
