package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import io.reactivex.Completable

interface VisitUser {

    fun visitUser(user: UserSearchResult): Completable
}