package com.corp.kenzoku.cruor.di

import com.corp.kenzoku.cruor.data.datasources.items.ItemDatasource
import com.corp.kenzoku.cruor.data.datasources.items.ItemDatasourceImpl
import com.corp.kenzoku.cruor.data.datasources.items.VisitedItemDatasource
import com.corp.kenzoku.cruor.data.datasources.items.VisitedItemDatasourceImpl
import com.corp.kenzoku.cruor.data.mappers.ItemEntityMapper
import com.corp.kenzoku.cruor.data.mappers.VisitedItemEntityMapper
import com.corp.kenzoku.cruor.data.repositories.ItemRepository
import com.corp.kenzoku.cruor.data.repositories.ItemRepositoryImpl
import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepositoryImpl
import com.corp.kenzoku.cruor.domain.usecases.*
import com.corp.kenzoku.cruor.presentation.models.mappers.SearchItemModelMapper
import com.corp.kenzoku.cruor.presentation.models.mappers.VisitedItemModelMapper
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val itemDatasourceModule = Kodein.Module {
    bind<ItemDatasource>() with provider { ItemDatasourceImpl(instance()) }
}

val visitedItemDatasourceModule = Kodein.Module {
    bind<VisitedItemDatasource>() with provider { VisitedItemDatasourceImpl(instance()) }
}

val itemEntityMapperModule = Kodein.Module {
    bind<ItemEntityMapper>() with provider { ItemEntityMapper() }
}

val visitedItemEntityMapperModule = Kodein.Module {
    bind<VisitedItemEntityMapper>() with provider { VisitedItemEntityMapper() }
}

val itemRepositoryModule = Kodein.Module {
    bind<ItemRepository>() with provider { ItemRepositoryImpl(instance(), instance()) }
}

val visitedItemRepositoryModule = Kodein.Module {
    bind<VisitedItemRepository>() with provider { VisitedItemRepositoryImpl(instance(), instance()) }
}

val loadUserByIdModule = Kodein.Module {
    bind<LoadUserById>() with provider { LoadUserByIdUseCase(instance()) }
}

val searchUserModule = Kodein.Module {
    bind<SearchUser>() with provider { SearchUserUseCase(instance(), instance()) }
}

val updateFavoriteModule = Kodein.Module {
    bind<UpdateFavorite>() with provider { UpdateFavoriteUseCase(instance()) }
}

val visitUserModule = Kodein.Module {
    bind<VisitUser>() with provider { VisitUserUseCase(instance()) }
}

val searchItemModelMapperModule = Kodein.Module {
    bind<SearchItemModelMapper>() with provider { SearchItemModelMapper() }
}

val visitedItemModelMapperModule = Kodein.Module {
    bind<VisitedItemModelMapper>() with provider { VisitedItemModelMapper() }
}