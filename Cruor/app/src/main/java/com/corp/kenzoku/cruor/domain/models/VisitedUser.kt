package com.corp.kenzoku.cruor.domain.models

data class VisitedUser(val id: String,
                       val avatar: String,
                       val username: String,
                       val profileUrl: String,
                       var isFavorite: Boolean)