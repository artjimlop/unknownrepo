package com.corp.kenzoku.cruor.presentation.views

import com.corp.kenzoku.cruor.presentation.models.SearchItemModel

interface SearchItemView : BaseView {
    fun showResults(searchResults: List<SearchItemModel>)
    fun goToItemDetail(item: SearchItemModel)
}