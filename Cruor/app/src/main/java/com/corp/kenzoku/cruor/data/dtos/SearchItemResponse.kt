package com.corp.kenzoku.cruor.data.dtos

data class SearchItemResponse(val items: List<ItemEntity>)