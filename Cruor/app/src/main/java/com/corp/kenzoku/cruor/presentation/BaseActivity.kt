package com.corp.kenzoku.cruor.presentation

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.corp.kenzoku.cruor.R
import org.kodein.di.Copy
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.android.retainedKodein

abstract class BaseActivity: AppCompatActivity(), KodeinAware {
    private val parentKodein by closestKodein()
    override val kodein: Kodein by retainedKodein {
        extend(parentKodein, copy = Copy.All)
    }
    internal var toolbar: Toolbar? = null

    fun setUpToolbar(toolbar: Toolbar?, showUpButton: Boolean) {
        this.toolbar = toolbar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(showUpButton)
            supportActionBar!!.setDisplayShowTitleEnabled(true)
        }
    }

    fun showErrorFeedback(rootView: View) {
        Snackbar.make(rootView, getString(R.string.error_message), Snackbar.LENGTH_SHORT).show()
    }
}
