package com.corp.kenzoku.cruor.presentation.presenters

import com.corp.kenzoku.cruor.domain.usecases.SearchUser
import com.corp.kenzoku.cruor.domain.usecases.VisitUser
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel
import com.corp.kenzoku.cruor.presentation.models.mappers.SearchItemModelMapper
import com.corp.kenzoku.cruor.presentation.views.SearchItemView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchItemPresenter constructor(private val searchUser: SearchUser,
                                      private val visitUser: VisitUser,
                                      private val searchItemModelMapper: SearchItemModelMapper) {

    private lateinit var view: SearchItemView

    fun initialize(searchItemView: SearchItemView) {
        this.view = searchItemView
    }

    fun searchItem(query: String) {
        searchUser.searchUser(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ searchResults ->
                    val itemModels = searchItemModelMapper.toModels(searchResults)
                    view.showResults(itemModels)
                }, { _ ->
                    view.showError()
                })
    }

    fun onItemClicked(item: SearchItemModel) {
        visitUser.visitUser(searchItemModelMapper.toBusinessObject(item))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.goToItemDetail(item)
                }, { _ ->
                    view.showError()
                })
    }
}