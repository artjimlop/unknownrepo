package com.corp.kenzoku.cruor.presentation.models.mappers

import com.corp.kenzoku.cruor.domain.models.VisitedUser
import com.corp.kenzoku.cruor.presentation.models.VisitedItemModel

class VisitedItemModelMapper {
    fun toModel(visitedUser: VisitedUser): VisitedItemModel {
        return VisitedItemModel(visitedUser.id, visitedUser.avatar, visitedUser.username,
                visitedUser.profileUrl, visitedUser.isFavorite)
    }
}