package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.data.repositories.ItemRepository
import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import com.corp.kenzoku.cruor.domain.models.User
import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.domain.models.VisitedUser
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class SearchUserUseCase constructor(private val itemRepository: ItemRepository,
                                    private val visitedItemRepository: VisitedItemRepository):
        SearchUser {

    override fun searchUser(query: String): Single<List<UserSearchResult>> {
        val users = itemRepository.searchUsers(query)
        val visitedUsers = visitedItemRepository.loadVisitedUsers()
        return buildResults(users, visitedUsers)
    }

    private fun buildResults(users: Single<List<User>>, visitedUsers: Single<List<VisitedUser>>): Single<List<UserSearchResult>> {
        return Single.zip(users, visitedUsers,
                BiFunction { t1, t2 -> combineUserInfo(t1, t2) })
    }

    private fun combineUserInfo(users: List<User>, visitedUsers: List<VisitedUser>): List<UserSearchResult> {
        val visitedUserIds: List<String> = getUserIds(visitedUsers)
        val favoriteUsersIds: List<String> = getFavoriteUserIds(visitedUsers)
        val resultList = users.map { it -> toModel(it, visitedUserIds, favoriteUsersIds) }
        markFirstResultAsBestMatch(resultList)
        return resultList
    }

    private fun markFirstResultAsBestMatch(resultList: List<UserSearchResult>) {
        if (resultList.isNotEmpty()) {
            resultList.first().isBestMatch = true
        }
    }

    private fun getFavoriteUserIds(t2: List<VisitedUser>): List<String> {
        val favoriteUsers: List<VisitedUser> = getFavoriteUsers(t2)
        return getUserIds(favoriteUsers)
    }

    private fun getFavoriteUsers(t2: List<VisitedUser>): List<VisitedUser> {
        return t2.filter { it.isFavorite }
    }

    private fun getUserIds(t2: List<VisitedUser>): List<String> {
        return t2.map { it -> it.id }
    }

    private fun toModel(user: User, visitedUserIds: List<String>, favoriteUsersIds: List<String>): UserSearchResult {
        return UserSearchResult(user.id, user.avatar, user.username, user.profileUrl, favoriteUsersIds.contains(user.id), visitedUserIds.contains(user.id))
    }
}