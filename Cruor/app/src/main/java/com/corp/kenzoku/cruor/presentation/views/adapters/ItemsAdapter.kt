package com.corp.kenzoku.cruor.presentation.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.corp.kenzoku.cruor.R
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel

class ItemsAdapter(private val listener: (SearchItemModel) -> Unit) : RecyclerView.Adapter<ItemViewHolder>() {
    private var items: List<SearchItemModel>? = null

    fun setItems(items: List<SearchItemModel>) {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ItemViewHolder {
        return ItemViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items!![position], listener)
    }

    override fun getItemCount(): Int {
        if (items != null) {
            return items!!.size
        }
        return 0
    }
}
