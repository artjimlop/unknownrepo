package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import io.reactivex.Completable

class UpdateFavoriteUseCase constructor(private val visitedItemRepository: VisitedItemRepository): UpdateFavorite {

    override fun updateFavorite(userId: String, isFavorite: Boolean): Completable {
        return visitedItemRepository.updateUserFavorite(userId, isFavorite)
    }
}

