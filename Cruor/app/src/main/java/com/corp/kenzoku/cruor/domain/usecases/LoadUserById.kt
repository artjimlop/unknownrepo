package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.domain.models.VisitedUser
import io.reactivex.Single

interface LoadUserById {

    fun loadUserById(userId: String): Single<VisitedUser>
}