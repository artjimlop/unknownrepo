package com.corp.kenzoku.cruor.presentation.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.corp.kenzoku.cruor.R
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item.view.*

class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: SearchItemModel, listener: (SearchItemModel) -> Unit) {
        with(item) {
            setListener(listener, item)
            setItemName(item)
            setupVisitedLabel(item)
            setupIsFavoriteLabel(item)
            setupBestMatchLabel(item)
            setupImage(item)
        }
    }

    private fun setListener(listener: (SearchItemModel) -> Unit, item: SearchItemModel) {
        itemView.setOnClickListener { listener(item) }
    }

    private fun setItemName(item: SearchItemModel) {
        itemView.itemName.text = item.username
    }

    private fun setupVisitedLabel(item: SearchItemModel) {
        if (item.alreadyVisited) {
            itemView.visited.visibility = VISIBLE
        } else {
            itemView.visited.visibility = GONE
        }
    }

    private fun setupIsFavoriteLabel(item: SearchItemModel) {
        if (item.isFavorite) {
            itemView.favorite.visibility = VISIBLE
        } else {
            itemView.favorite.visibility = GONE
        }
    }

    private fun setupBestMatchLabel(item: SearchItemModel) {
        if (item.isBestMatch) {
            itemView.bestMatchLabel.visibility = VISIBLE
        } else {
            itemView.bestMatchLabel.visibility = GONE
        }
    }

    private fun setupImage(item: SearchItemModel) {
        Picasso.get().load(item.avatar).placeholder(R.drawable.ic_launcher_background).fit().centerCrop().into(itemView.itemImage)
    }
}

