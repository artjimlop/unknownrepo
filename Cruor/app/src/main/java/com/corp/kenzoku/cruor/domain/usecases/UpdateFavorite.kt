package com.corp.kenzoku.cruor.domain.usecases

import io.reactivex.Completable
import io.reactivex.Observable

interface UpdateFavorite {

    fun updateFavorite(userId: String, isFavorite: Boolean): Completable
}
