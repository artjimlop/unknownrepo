package com.corp.kenzoku.cruor.data.mappers

import com.corp.kenzoku.cruor.data.dtos.VisitedItemEntity
import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.domain.models.VisitedUser

class VisitedItemEntityMapper {

    fun toEntity(user: UserSearchResult): VisitedItemEntity {
        return VisitedItemEntity(user.id, user.avatar, user.username, user.profileUrl, user.isFavorite)
    }

    fun toBusinessObject(visitedItem: VisitedItemEntity): VisitedUser {
        return VisitedUser(visitedItem.id, visitedItem.avatar, visitedItem.username, visitedItem.profileUrl,
                visitedItem.isFavorite)
    }

    fun toBusinessObjects(visitedItems: List<VisitedItemEntity>): List<VisitedUser> {
        return visitedItems.map { it -> toBusinessObject(it) }
    }
}