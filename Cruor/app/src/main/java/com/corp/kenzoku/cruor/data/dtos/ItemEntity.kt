package com.corp.kenzoku.cruor.data.dtos

data class ItemEntity(val id: String,
                      val avatar_url: String,
                      val login: String,
                      val html_url: String)