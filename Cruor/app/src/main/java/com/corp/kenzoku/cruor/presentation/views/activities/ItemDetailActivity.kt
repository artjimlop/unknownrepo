package com.corp.kenzoku.cruor.presentation.views.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import com.corp.kenzoku.cruor.R
import com.corp.kenzoku.cruor.presentation.BaseActivity
import com.corp.kenzoku.cruor.presentation.models.VisitedItemModel
import com.corp.kenzoku.cruor.presentation.presenters.ItemDetailPresenter
import com.corp.kenzoku.cruor.presentation.views.ItemDetailView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_item_detail.*
import kotlinx.android.synthetic.main.toolbar.view.*
import org.kodein.di.generic.instance

class ItemDetailActivity : BaseActivity(), ItemDetailView {
    private val presenter: ItemDetailPresenter by instance()

    companion object {
        const val EXTRA_ITEM_ID: String = "extra_item_id"

        fun goToItemDetail(context: Context, itemId: String): Intent {
            val intent = Intent(context, ItemDetailActivity::class.java)
            intent.putExtra(EXTRA_ITEM_ID, itemId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        setUpToolbar(detailAppBar.toolbar, true)
        val itemId = intent.getStringExtra(EXTRA_ITEM_ID)
        presenter.initialize(this, itemId)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showItemInfo(visitedItemModel: VisitedItemModel) {
        setupItemName(visitedItemModel)
        setupImage(visitedItemModel)
        setupOpenInBrowserListener(visitedItemModel)
        setupFavoriteView(visitedItemModel)
    }

    private fun setupFavoriteView(visitedItem: VisitedItemModel) {
        favoriteView.isLiked = visitedItem.isFavorite
        favoriteView.setOnClickListener {
            presenter.onFavoriteClicked(visitedItem, !favoriteView.isLiked)
        }
    }

    override fun switchFavoriteStatus() {
        favoriteView.isLiked = !favoriteView.isLiked
    }

    private fun setupOpenInBrowserListener(visitedItem: VisitedItemModel) {
        openInBrowserButton.setOnClickListener {
            getOpenInBrowserIntent(visitedItem)
        }
    }

    private fun getOpenInBrowserIntent(visitedItem: VisitedItemModel) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(visitedItem.profileUrl)
        startActivity(intent)
    }

    private fun setupImage(visitedItem: VisitedItemModel) {
        Picasso.get().load(visitedItem.avatar).placeholder(R.drawable.ic_launcher_background).fit().centerCrop().into(itemDetailImage)
    }

    private fun setupItemName(visitedItem: VisitedItemModel) {
        itemName.text = visitedItem.username
    }

    override fun showError() {
        showErrorFeedback(detailRootView)
    }
}
