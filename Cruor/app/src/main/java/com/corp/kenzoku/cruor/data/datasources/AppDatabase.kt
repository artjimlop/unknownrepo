package com.corp.kenzoku.cruor.data.datasources

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.corp.kenzoku.cruor.data.daos.VisitedItemsDAO
import com.corp.kenzoku.cruor.data.dtos.VisitedItemEntity

@Database(entities = [(VisitedItemEntity::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun visitedItemDAO(): VisitedItemsDAO
}