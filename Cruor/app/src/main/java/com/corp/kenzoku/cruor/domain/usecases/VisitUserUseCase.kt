package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import io.reactivex.Completable

class VisitUserUseCase constructor(private val visitedItemRepository: VisitedItemRepository):
        VisitUser {

    override fun visitUser(user: UserSearchResult): Completable {
        return visitedItemRepository.markUserAsVisited(user)
    }
}