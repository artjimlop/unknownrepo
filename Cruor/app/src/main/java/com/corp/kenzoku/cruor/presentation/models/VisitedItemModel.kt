package com.corp.kenzoku.cruor.presentation.models

data class VisitedItemModel(val id: String,
                            val avatar: String,
                            val username: String,
                            val profileUrl: String,
                            var isFavorite: Boolean)
