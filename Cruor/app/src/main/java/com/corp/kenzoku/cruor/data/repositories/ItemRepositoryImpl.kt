package com.corp.kenzoku.cruor.data.repositories

import com.corp.kenzoku.cruor.data.datasources.items.ItemDatasource
import com.corp.kenzoku.cruor.data.mappers.ItemEntityMapper
import com.corp.kenzoku.cruor.domain.models.User
import io.reactivex.Single

class ItemRepositoryImpl constructor(private val itemDatasource: ItemDatasource,
                                     private val itemEntityMapper: ItemEntityMapper):
        ItemRepository {

    override fun searchUsers(query: String): Single<List<User>> {
        val users = itemDatasource.searchItem(query)
        return users.map { itemEntityMapper.toBusinessObject(it) }
    }
}