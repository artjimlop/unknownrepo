package com.corp.kenzoku.cruor.data.repositories

import com.corp.kenzoku.cruor.data.datasources.items.VisitedItemDatasource
import com.corp.kenzoku.cruor.data.mappers.VisitedItemEntityMapper
import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.domain.models.VisitedUser
import io.reactivex.Completable
import io.reactivex.Single

class VisitedItemRepositoryImpl constructor(private val visitedItemDatasource: VisitedItemDatasource,
                                            private val visitedItemEntityMapper: VisitedItemEntityMapper):
        VisitedItemRepository {

    override fun markUserAsVisited(user: UserSearchResult): Completable {
        val visitedUserEntity = visitedItemEntityMapper.toEntity(user)
        return visitedItemDatasource.markItemAsVisited(visitedUserEntity)
    }

    override fun loadVisitedUser(userId: String): Single<VisitedUser> {
        val visitedUser = visitedItemDatasource.loadItemById(userId)
        return visitedUser.map { visitedItemEntityMapper.toBusinessObject(it) }
    }

    override fun loadVisitedUsers(): Single<List<VisitedUser>> {
        val visitedUser = visitedItemDatasource.loadVisitedItems()
        return visitedUser.map { visitedItemEntityMapper.toBusinessObjects(it) }
    }

    override fun updateUserFavorite(userId: String, isFavorite: Boolean): Completable {
        return visitedItemDatasource.updateItemFavorite(userId, isFavorite)
    }
}