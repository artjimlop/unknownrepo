package com.corp.kenzoku.cruor.presentation.models.mappers

import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel

class SearchItemModelMapper {
    fun toModels(users: List<UserSearchResult>): List<SearchItemModel> {
        return users.map { it -> toModel(it) }
    }

    private fun toModel(userSearchResult: UserSearchResult): SearchItemModel {
        return SearchItemModel(userSearchResult.id,
                userSearchResult.avatar, userSearchResult.username,
                userSearchResult.profileUrl, userSearchResult.isFavorite,
                userSearchResult.alreadyVisited, userSearchResult.isBestMatch)
    }

    fun toBusinessObject(searchItemModel: SearchItemModel): UserSearchResult {
        return UserSearchResult(searchItemModel.id, searchItemModel.avatar,
                searchItemModel.username, searchItemModel.profileUrl, searchItemModel.isFavorite,
                searchItemModel.alreadyVisited, searchItemModel.isBestMatch)
    }

}