package com.corp.kenzoku.cruor.data.repositories

import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.domain.models.VisitedUser
import io.reactivex.Completable
import io.reactivex.Single

interface VisitedItemRepository {

    fun markUserAsVisited(user: UserSearchResult): Completable
    fun loadVisitedUser(userId: String): Single<VisitedUser>
    fun loadVisitedUsers(): Single<List<VisitedUser>>
    fun updateUserFavorite(userId: String, isFavorite: Boolean): Completable
}