package com.corp.kenzoku.cruor.data.datasources.items

import com.corp.kenzoku.cruor.data.dtos.SearchItemResponse
import io.reactivex.Single

interface ItemDatasource {

    fun searchItem(query: String): Single<SearchItemResponse>
}