package com.corp.kenzoku.cruor.di

import android.arch.persistence.room.Room
import com.corp.kenzoku.cruor.GitHubApplication
import com.corp.kenzoku.cruor.data.daos.VisitedItemsDAO
import com.corp.kenzoku.cruor.data.datasources.AppDatabase
import com.corp.kenzoku.cruor.data.net.ApiConstants
import com.corp.kenzoku.cruor.data.net.ItemApiService
import com.corp.kenzoku.cruor.presentation.presenters.ItemDetailPresenter
import com.corp.kenzoku.cruor.presentation.presenters.SearchItemPresenter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun injector(gitHubApplication: GitHubApplication): Kodein {
    return Kodein.lazy {
        import(searchUserModule)
        import(visitUserModule)
        import(loadUserByIdModule)
        import(updateFavoriteModule)
        import(itemRepositoryModule)
        import(itemDatasourceModule)
        import(itemEntityMapperModule)
        import(visitedItemRepositoryModule)
        import(visitedItemDatasourceModule)
        import(visitedItemEntityMapperModule)
        import(visitedItemModelMapperModule)
        import(searchItemModelMapperModule)

        bind<String>() with instance("GitHubApplication")
        bind<SearchItemPresenter>() with provider { SearchItemPresenter(instance(), instance(), instance()) }
        bind<ItemDetailPresenter>() with provider { ItemDetailPresenter(instance(), instance(), instance()) }

        val retrofit = setupAPIClient()
        bind<ItemApiService>() with singleton {
            retrofit.create(ItemApiService::class.java)
        }

        val database =
                Room.databaseBuilder(gitHubApplication, AppDatabase::class.java, "items_database").build()
        bind<VisitedItemsDAO>() with singleton {
            database.visitedItemDAO()
        }
    }
}


fun setupAPIClient(): Retrofit {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val httpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor)
            .build()

    return Retrofit.Builder()
            .baseUrl(ApiConstants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}