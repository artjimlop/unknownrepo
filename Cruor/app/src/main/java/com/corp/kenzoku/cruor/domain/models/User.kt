package com.corp.kenzoku.cruor.domain.models

data class User(val id: String,
                val avatar: String,
                val username: String,
                val profileUrl: String)