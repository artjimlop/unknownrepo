package com.corp.kenzoku.cruor.data.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.corp.kenzoku.cruor.data.dtos.VisitedItemEntity

@Dao
interface VisitedItemsDAO {

    @Query("SELECT * FROM visited_items")
    fun loadAllItems(): List<VisitedItemEntity>

    @Query("select * from visited_items where id = :id")
    fun findItemById(id: String): VisitedItemEntity

    @Query("UPDATE visited_items SET isFavorite = :isFavorite  WHERE id = :id")
    fun updateFavorite(id: String, isFavorite: Boolean)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItem(visitedItem: VisitedItemEntity)

}