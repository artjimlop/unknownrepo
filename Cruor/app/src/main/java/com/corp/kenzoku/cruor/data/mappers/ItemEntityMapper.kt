package com.corp.kenzoku.cruor.data.mappers

import com.corp.kenzoku.cruor.data.dtos.ItemEntity
import com.corp.kenzoku.cruor.data.dtos.SearchItemResponse
import com.corp.kenzoku.cruor.domain.models.User

class ItemEntityMapper {
    fun toBusinessObject(searchResponse: SearchItemResponse): List<User> {
        return searchResponse.items.map { it -> transform(it) }
    }

    private fun transform(itemEntity: ItemEntity) =
            User(itemEntity.id, itemEntity.avatar_url, itemEntity.login, itemEntity.html_url)

}