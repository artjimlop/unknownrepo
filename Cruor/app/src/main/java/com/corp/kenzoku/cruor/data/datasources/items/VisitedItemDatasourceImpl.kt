package com.corp.kenzoku.cruor.data.datasources.items

import com.corp.kenzoku.cruor.data.daos.VisitedItemsDAO
import com.corp.kenzoku.cruor.data.dtos.VisitedItemEntity
import io.reactivex.Completable
import io.reactivex.Single

class VisitedItemDatasourceImpl constructor(private val visitedItemsDAO: VisitedItemsDAO) :
        VisitedItemDatasource {

    override fun markItemAsVisited(visitedItemEntity: VisitedItemEntity): Completable {
        return Completable.create { emitter ->
            visitedItemsDAO.insertItem(visitedItemEntity)
            emitter.onComplete()
        }
    }

    override fun loadItemById(itemId: String): Single<VisitedItemEntity> {
        return Single.create<VisitedItemEntity> { it.onSuccess(visitedItemsDAO.findItemById(itemId)) }
    }

    override fun loadVisitedItems(): Single<List<VisitedItemEntity>> {
        return Single.create<List<VisitedItemEntity>> { it.onSuccess(visitedItemsDAO.loadAllItems()) }
    }

    override fun updateItemFavorite(itemId: String, isFavorite: Boolean): Completable {
        return Completable.create { emitter ->
            visitedItemsDAO.updateFavorite(itemId, isFavorite)
            emitter.onComplete()
        }
    }
}