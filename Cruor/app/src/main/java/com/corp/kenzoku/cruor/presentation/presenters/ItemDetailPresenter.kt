package com.corp.kenzoku.cruor.presentation.presenters

import com.corp.kenzoku.cruor.domain.usecases.LoadUserById
import com.corp.kenzoku.cruor.domain.usecases.UpdateFavorite
import com.corp.kenzoku.cruor.presentation.models.VisitedItemModel
import com.corp.kenzoku.cruor.presentation.models.mappers.VisitedItemModelMapper
import com.corp.kenzoku.cruor.presentation.views.ItemDetailView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ItemDetailPresenter constructor(private val loadUserById: LoadUserById,
                                      private val updateFavorite: UpdateFavorite,
                                      private val visitedItemModelMapper: VisitedItemModelMapper) {

    private lateinit var view: ItemDetailView

    fun initialize(itemDetailView: ItemDetailView, userId: String) {
        this.view = itemDetailView
        loadUser(userId)
    }

    private fun loadUser(userId: String) {
        loadUserById.loadUserById(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ visited ->
                    val visitedItemModel = visitedItemModelMapper.toModel(visited)
                    view.showItemInfo(visitedItemModel)
                }, { _ ->
                    view.showError()
                })
    }

    fun onFavoriteClicked(visitedItem: VisitedItemModel, isFavorite: Boolean) {
        updateFavorite.updateFavorite(visitedItem.id, isFavorite)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.switchFavoriteStatus()
                }, { _ ->
                    view.switchFavoriteStatus()
                    view.showError()
                })
    }
}
