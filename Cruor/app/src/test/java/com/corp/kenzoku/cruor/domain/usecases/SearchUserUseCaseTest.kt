package com.corp.kenzoku.cruor.domain.usecases

import com.corp.kenzoku.cruor.RxImmediateSchedulerRule
import com.corp.kenzoku.cruor.data.repositories.ItemRepository
import com.corp.kenzoku.cruor.data.repositories.VisitedItemRepository
import com.corp.kenzoku.cruor.domain.models.User
import com.corp.kenzoku.cruor.domain.models.VisitedUser
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class SearchUserUseCaseTest {

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }
    private val query = "query"
    private lateinit var searchUserUseCase: SearchUserUseCase
    private lateinit var itemRepository: ItemRepository
    private lateinit var visitedItemRepository: VisitedItemRepository

    @Before
    fun setUp() {
        itemRepository = Mockito.mock(ItemRepository::class.java)
        visitedItemRepository = Mockito.mock(VisitedItemRepository::class.java)
        searchUserUseCase = SearchUserUseCase(itemRepository, visitedItemRepository)
    }

    @Test
    fun searchesUsersByQueryInUserRepositoryWhenSearchingUser() {
        whenever(itemRepository.searchUsers(any())).thenReturn(getEmptyUserSingle())
        whenever(visitedItemRepository.loadVisitedUsers()).thenReturn(getEmptyVisitedUserSingle())

        searchUserUseCase.searchUser(query)

        verify(itemRepository).searchUsers(query)
    }

    @Test
    fun loadVisitedUsersWhenSearchingUser() {
        whenever(itemRepository.searchUsers(any())).thenReturn(getEmptyUserSingle())
        whenever(visitedItemRepository.loadVisitedUsers()).thenReturn(getEmptyVisitedUserSingle())

        searchUserUseCase.searchUser(query)

        verify(visitedItemRepository).loadVisitedUsers()
    }

    private fun getEmptyUserSingle(): Single<List<User>>? {
        val data: List<User> = ArrayList()
        return Single.just(data)
    }

    private fun getEmptyVisitedUserSingle(): Single<List<VisitedUser>>? {
        val data: List<VisitedUser> = ArrayList()
        return Single.just(data)
    }
}