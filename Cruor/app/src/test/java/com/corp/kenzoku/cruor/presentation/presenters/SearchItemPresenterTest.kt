package com.corp.kenzoku.cruor.presentation.presenters

import com.corp.kenzoku.cruor.RxImmediateSchedulerRule
import com.corp.kenzoku.cruor.domain.models.UserSearchResult
import com.corp.kenzoku.cruor.domain.usecases.SearchUser
import com.corp.kenzoku.cruor.domain.usecases.VisitUser
import com.corp.kenzoku.cruor.presentation.models.SearchItemModel
import com.corp.kenzoku.cruor.presentation.models.mappers.SearchItemModelMapper
import com.corp.kenzoku.cruor.presentation.views.SearchItemView
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.mockito.Mockito
import java.lang.RuntimeException
import java.util.*

class SearchItemPresenterTest {

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    private val query = "query"
    private lateinit var searchItemPresenter: SearchItemPresenter
    private lateinit var searchUser: SearchUser
    private lateinit var visitUser: VisitUser
    private lateinit var view: SearchItemView

    @Before
    fun setUp() {
        searchUser = Mockito.mock(SearchUser::class.java)
        visitUser = Mockito.mock(VisitUser::class.java)
        view = Mockito.mock(SearchItemView::class.java)
        val searchItemModelMapper = SearchItemModelMapper()
        searchItemPresenter = SearchItemPresenter(searchUser, visitUser, searchItemModelMapper)
        searchItemPresenter.initialize(view)
    }

    @Test
    fun showResultsWhenQuerySearched() {
        whenever(searchUser.searchUser(any())).thenReturn(getEmptyUserSingle())

        searchItemPresenter.searchItem(query)

        verify(view).showResults(any())
    }

    @Test
    fun goToItemDetailWhenVisitUser() {
        whenever(visitUser.visitUser(any())).thenReturn(getVisitUserObservable())

        searchItemPresenter.onItemClicked(getSearchItemModel())

        verify(view).goToItemDetail(any())
    }

    @Test
    fun showErrorInViewWhenReceiveErrorOnObservable() {
        whenever(visitUser.visitUser(any())).thenReturn(getVisitUserErrorObservable())

        searchItemPresenter.onItemClicked(getSearchItemModel())

        verify(view).showError()
    }

    private fun getSearchItemModel(): SearchItemModel {
        return SearchItemModel("","","", "", false, false)
    }

    private fun getEmptyUserSingle(): Single<List<UserSearchResult>>? {
        return Single.just(ArrayList())
    }

    private fun getVisitUserObservable(): Completable {
        return Completable.complete()
    }

    private fun getVisitUserErrorObservable(): Completable {
        return Completable.error(RuntimeException())
    }
}