## App description:

* You land in an empty activity.

* Hit the search button.

* Search any username on Github.

* Get the info of all users matching your query.

* The info displayed at the list: username, picture, visited status, favorite status

* Visited status? When you click on the user, you go to user's detail. The user must be marked as visited.
This is achieved using Room.

* Favorite status? You can save the user as favorite at its detail screen.

## Architectural approach:

* 3 layers - presentation, domain, data.

* DI - using Kodein because it's lightweight

* Tests - a presenter and a use case are tested. The rest can be tested in a similar way.